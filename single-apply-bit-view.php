<?php include('header.php'); ?>
<section id="login-id">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="single-job-view">
                        <div class="card">
                            <div class="card-header">
                                <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>
                            </div>
                            <div class="card-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis aut quam debitis deserunt ut impedit officia doloribus eos ducimus? Reiciendis nesciunt est magni quasi maxime harum quam, sapiente adipisci nihil totam, quo! Magni delectus, quidem quo quibusdam illo fugiat possimus animi quam dolorem tenetur inventore mollitia reiciendis soluta voluptate ipsam!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis aut quam debitis deserunt ut impedit officia doloribus eos ducimus? Reiciendis nesciunt est magni quasi maxime harum quam, sapiente adipisci nihil totam, quo! Magni delectus, quidem quo quibusdam illo fugiat possimus animi quam dolorem tenetur inventore mollitia reiciendis soluta voluptate ipsam!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis aut quam debitis deserunt ut impedit officia doloribus eos ducimus? Reiciendis nesciunt est magni quasi maxime harum quam, sapiente adipisci nihil totam, quo! Magni delectus, quidem quo quibusdam illo fugiat possimus animi quam dolorem tenetur inventore mollitia reiciendis soluta voluptate ipsam!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis aut quam debitis deserunt ut impedit officia doloribus eos ducimus? Reiciendis nesciunt est magni quasi maxime harum quam, sapiente adipisci nihil totam, quo! Magni delectus, quidem quo quibusdam illo fugiat possimus animi quam dolorem tenetur inventore mollitia reiciendis soluta voluptate ipsam!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis aut quam debitis deserunt ut impedit officia doloribus eos ducimus? Reiciendis nesciunt est magni quasi maxime harum quam, sapiente adipisci nihil totam, quo! Magni delectus, quidem quo quibusdam illo fugiat possimus animi quam dolorem tenetur inventore mollitia reiciendis soluta voluptate ipsam!</p>
                            </div>
                            <div class="card-footer">
                                <div class="job-type-info">
                                    <h6>Job Type: One Time</h6>
                                    <h6>Rate Type: Hourly</h6>
                                    <h6>Skill Query: Expert</h6>
                                </div>
                                <hr>
                                <div class="skills-serial">
                                    <h6>Refer Lists: </h6>
                                    <p>HTML</p>
                                    <p>CSS</p>
                                    <p>JS</p>
                                    <p>PHP</p>
                                    <p>ANGILAR</p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="proposel">
                        <div class="card">
                            <div class="row justify-content-center">
                                <div class="col-lg-8 text-center">
                                    <a href="#" class="proposal-submit">Send Message</a>
                                    <button class="btn waves-effect waves-light" type="submit" name="action">Approved Proposel</button>
                                </div>
                            </div>
                            <hr>

                            <div class="client-summery">
                                <h6>About Client</h6>
                                <p>1 Job Posted</p>
                                <p>Applyed: 32 Person</p>
                            </div>

                            <div class="chat-box">
                                <div class="card">
                                    <div class="row justify-content-center">
                                        <div class="col-lg-10 frame">
                                            <ul></ul>
                                            <div>
                                                <div class="msj-rta macro">
                                                    <div class="text text-r" style="background:whitesmoke !important">
                                                        <input class="mytext" placeholder="Type a message" />
                                                    </div>

                                                </div>
                                                <div style="padding:10px;">
                                                    <span class="glyphicon glyphicon-share-alt"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>



<!--    [ Strat Section Area]-->
<link rel="stylesheet" href="assets/css/chat-box.css">
<!--<div class="row">
    <div class="col-sm-3 col-sm-offset-4 frame">

    </div>
</div>-->


<!--    [Finish Section Area]-->


<?php include('footer.php'); ?>
<script>
    var me = {};
    me.avatar = "https://lh6.googleusercontent.com/-lr2nyjhhjXw/AAAAAAAAAAI/AAAAAAAARmE/MdtfUmC0M4s/photo.jpg?sz=48";

    var you = {};
    you.avatar = "https://a11.t26.net/taringa/avatares/9/1/2/F/7/8/Demon_King1/48x48_5C5.jpg";

    function formatAMPM(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    }

    //-- No use time. It is a javaScript effect.
    function insertChat(who, text, time) {
        if (time === undefined) {
            time = 0;
        }
        var control = "";
        var date = formatAMPM(new Date());

        if (who == "me") {
            control = '<li style="width:100%">' +
                '<div class="msj macro">' +
                '<div class="avatar"><img class="img-circle" style="width:100%;" src="' + me.avatar + '" /></div>' +
                '<div class="text text-l">' +
                '<p>' + text + '</p>' +
                '<p><small>' + date + '</small></p>' +
                '</div>' +
                '</div>' +
                '</li>';
        } else {
            control = '<li style="width:100%;">' +
                '<div class="msj-rta macro">' +
                '<div class="text text-r">' +
                '<p>' + text + '</p>' +
                '<p><small>' + date + '</small></p>' +
                '</div>' +
                '<div class="avatar" style="padding:0px 0px 0px 10px !important"><img class="img-circle" style="width:100%;" src="' + you.avatar + '" /></div>' +
                '</li>';
        }
        setTimeout(
            function() {
                $("ul").append(control).scrollTop($("ul").prop('scrollHeight'));
            }, time);

    }

    function resetChat() {
        $("ul").empty();
    }

    $(".mytext").on("keydown", function(e) {
        if (e.which == 13) {
            var text = $(this).val();
            if (text !== "") {
                insertChat("me", text);
                $(this).val('');
            }
        }
    });

    $('body > div > div > div:nth-child(2) > span').click(function() {
        $(".mytext").trigger({
            type: 'keydown',
            which: 13,
            keyCode: 13
        });
    })

    //-- Clear Chat
    resetChat();

    //-- Print Messages
    insertChat("me", "Hello Tom...", 0);
    insertChat("you", "Hi, Pablo", 1500);
    insertChat("me", "What would you like to talk about today?", 3500);
    insertChat("you", "Tell me a joke", 7000);
    insertChat("me", "Spaceman: Computer! Computer! Do we bring battery?!", 9500);
    insertChat("you", "LOL", 12000);


    //-- NOTE: No use time on insertChat.

</script>

<script>
    $(document).ready(function() {
        $("#hide").click(function() {
            $("p").hide();
        });
        $("#show").click(function() {
            $("p").show();
        });
    });

</script>
