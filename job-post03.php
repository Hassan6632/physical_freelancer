<?php include('header.php'); ?>


<!--    [ Strat Section Title Area]-->
<link rel="stylesheet" href="assets/css/skills.css">


<section id="login-id">
    <div class="section-paddings">
        <div class="container">
            <div class="login-all-content">
                <div class="log-frm d-table">
                    <div class="log-frm-cont d-table-cell">
                        <div class="row justify-content-center">
                            <div class="col-lg-9">
                                <div class="login-content">
                                    <div class="card">
                                        <div class="card-header text-center">
                                            <h4>WELCOME TO <img src="assets/img/prelab.png" /></h4>
                                        </div>
                                        <div class="card-bpdy">
                                            <form action="" method="post">
                                                <div class="log-frm-fild">
                                                    <div class="row justify-content-center">
                                                        <div class="col-lg-8">
                                                            <div class="all-aply-fild">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="skills-list">
                                                                            <div class="input-field">
                                                                                <i class="icofont prefix">heading</i>
                                                                                <input id="heading-title" type="text" class="validate">
                                                                                <label for="heading-title">Job Title</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12">
                                                                        <div class="skills-list">
                                                                            <div class="input-field">
                                                                                <textarea id="textarea2" class="materialize-textarea" data-length="500"></textarea>
                                                                                <label for="textarea2">Job Description</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12">
                                                                        <div class="single-fild agree-fild">
                                                                            <div class="input-field">
                                                                                <i class="icofont prefix">location_pin</i>
                                                                                <input id="location" type="text" class="validate">
                                                                                <label for="location">Location Area</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12">
                                                                        <div class="skills-list">
                                                                            <div class="input-field col s12">
                                                                                <select>
                                                                              <option value="" disabled selected>Choose option</option>
                                                                              <option value="1">One Time</option>
                                                                              <option value="2">Project Basis</option>
                                                                              <option value="3">Long Time</option>
                                                                            </select>
                                                                                <label>Job Type</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12">
                                                                        <div class="skills-list">
                                                                            <div id="output"></div>
                                                                            <main>
                                                                                <article class="dropdown-article">
                                                                                    <p>Job Related Skills</p>
                                                                                    <div class="element-container">
                                                                                        <div class="dropdown">
                                                                                            <div class="selected_list">
                                                                                            </div>
                                                                                            <input type="text" data-value="" value="" placeholder="Select Skills" readonly />
                                                                                            <div class="options_list">
                                                                                                <div class="option" data-value="1">
                                                                                                    first
                                                                                                </div>
                                                                                                <div class="option" data-value="2">second</div>
                                                                                                <div class="option" data-value="3">third</div>
                                                                                                <div class="option" data-value="4">fourth</div>
                                                                                                <div class="option" data-value="5">fifth</div>
                                                                                                <div class="option" data-value="6">sixth</div>
                                                                                                <div class="option" data-value="7">seven</div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </article>
                                                                            </main>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <div class="single-fild agree-fild">
                                                                            <div class="input-field">
                                                                                <i class="icofont prefix">location_pin</i>
                                                                                <time-date-picker ng-model="dateValue"></time-date-picker>
                                                                                <label for="">Project Expect Time</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <div class="single-fild agree-fild">
                                                                            <div class="input-field">
                                                                                <i class="icofont prefix">location_pin</i>
                                                                                <input id="location" type="text" class="validate">
                                                                                <label for="location">Location Area</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-6 text-center">
                                                                        <div class="single-fild">
                                                                            <button type="submit" name="login">Additional Question</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row justify-content-center">
                                                                    <div class="col-lg-4 text-center">
                                                                        <div class="single-fild">
                                                                            <button type="submit" name="login">Submit</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <div class="single-fild agree-fild">
                                                                <div class="input-field">
                                                                    <i class="icofont prefix">optic</i>
                                                                    <select>
                                                                              <option value="" disabled selected>Job Type</option>
                                                                              <option value="1">Physical</option>
                                                                              <option value="2">Online</option>
                                                                        </select>
                                                                </div>
                                                            </div>
                                                            <div class="single-fild agree-fild">
                                                                <div class="file-field input-field">
                                                                    <div class="btn">
                                                                        <span>Additional File</span>
                                                                        <input type="file">
                                                                    </div>
                                                                    <div class="file-path-wrapper">
                                                                        <input class="file-path validate" type="text">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="single-fild agree-fild">
                                                                <div class="input-field">
                                                                    <i class="icofont prefix">money_bag</i>
                                                                    <input id="rate" type="text" class="validate">
                                                                    <label for="rate">Rate</label>
                                                                </div>
                                                            </div>
                                                            <div class="single-fild agree-fild">
                                                                <div class="input-field">
                                                                    <i class="icofont prefix">optic</i>
                                                                    <select>
                                                                              <option value="" disabled selected>Rate Type</option>
                                                                              <option value="1">Hourly</option>
                                                                              <option value="2">Fix</option>
                                                                        </select>
                                                                </div>
                                                            </div>

                                                            <div class="single-fild agree-fild">
                                                                <div class="input-field">
                                                                    <i class="icofont prefix">ui_call</i>
                                                                    <input id="contact" type="text" class="validate">
                                                                    <label for="contact">Contact</label>
                                                                </div>
                                                            </div>
                                                            <div class="single-fild agree-fild">
                                                                <div class="input-field">
                                                                    <i class="icofont prefix">numbered</i>
                                                                    <input id="nmbr" type="number" class="validate">
                                                                    <label for="nmbr">Number of Freelancer</label>
                                                                </div>
                                                            </div>
                                                            <div class="single-fild agree-fild">
                                                                <div class="input-field">
                                                                    <select>
                                                                          <option value="" disabled selected>Skill Level</option>
                                                                          <option value="1">Intermediate</option>
                                                                          <option value="2">Mid Level</option>
                                                                          <option value="3">Expert</option>
                                                                    </select>
                                                                </div>
                                                            </div>



                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Title Area]-->
<div class="container">
    <div class="row justify-content-center">
        <div class="cil-lg-4">
            <input type='text' id='timepicker-actions-exmpl' />
        </div>
    </div>
</div>

<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->
<?php include('footer.php'); ?>

<script>
    // Create start date
    var start = new Date(),
        prevDay,
        startHours = 9;

    // 09:00 AM
    start.setHours(9);
    start.setMinutes(0);

    // If today is Saturday or Sunday set 10:00 AM
    if ([6, 0].indexOf(start.getDay()) != -1) {
        start.setHours(10);
        startHours = 10
    }

    $('#timepicker-actions-exmpl').datepicker({
        timepicker: true,
        language: 'en',
        startDate: start,
        minHours: startHours,
        maxHours: 18,
        onSelect: function(fd, d, picker) {
            // Do nothing if selection was cleared
            if (!d) return;

            var day = d.getDay();

            // Trigger only if date is changed
            if (prevDay != undefined && prevDay == day) return;
            prevDay = day;

            // If chosen day is Saturday or Sunday when set
            // hour value for weekends, else restore defaults
            if (day == 6 || day == 0) {
                picker.update({
                    minHours: 10,
                    maxHours: 16
                })
            } else {
                picker.update({
                    minHours: 9,
                    maxHours: 18
                })
            }
        }
    })

</script>

<script>
    $(document).ready(function() {
        $('input#input_text, textarea#textarea2').characterCounter();
    });

    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems, options);
    });

    // Or with jQuery

    $(document).ready(function() {
        $('select').formSelect();
    });

</script>
<script>
    $(document).ready(function() {
        $('.dropdown').dropdown({
            multi_select: true
        });
    });

    $.fn.dropdown = function(options) {
        var $input = this.find('input'),
            $options_list = this.find('.options_list'),
            $seleted_list = $options_list.siblings('.selected_list'),
            settings = $.extend({}, {
                multi_select: false
            }, options);
        $input.on('click', function() {
            $options_list
                .slideDown("fast");
        });

        $options_list.on('click', '.option', function() {
            var $selected_option = $(this),
                data_value = $(this).attr('data-value'),
                data_text = $(this).text().trim();
            $input
                .attr('data-value', data_value)
                .val(data_text);
            if (settings.multi_select) {
                var $item = $('<div class="item" data-value=""><span class="text"></span><span class="remove_item">x</span></div>');
                $item
                    .attr('data-value', data_value)
                    .find('.text')
                    .text(data_text);
                $seleted_list.append($item);
                $selected_option.remove();
                $options_list
                    .siblings('input')
                    .attr("data-value", "")
                    .val("");
            }
            $options_list.slideUp("fast");
        });

        $seleted_list.off('click').on('click', '.item .remove_item', function() {
            var $clicked_item = $(this).parents('.item'),
                item_text = $clicked_item
                .find('.text')
                .text()
                .trim(),
                item_data_value = $clicked_item.attr('data-value'),
                $item = $('<div class="option" data-value="' + item_data_value + '">' + item_text + '</div>');
            $clicked_item.addClass('removed_item');
            setTimeout(function() {
                $options_list.append($item);
                $clicked_item.remove();
            }, 500);
        });
        return this;
    }

    $.fn.selectedList = function() {
        var list = [];
        this.find('.selected_list .item').each(function(ind, option) {
            list.push({
                key: $(option).attr('data-value'),
                value: $(option).find('.text').text().trim()
            })
        });
        return list;
    };

</script>
