<?php include('header.php'); ?>

<style>
    input:focus,
    input:active {
        outline: none;
        outline-offset: 0;
    }

    .dropdown {
        position: relative;
        width: 100%;

    }

    .dropdown input {
        background-color: #009688;
        border: 1px solid #009688;
        text-transform: capitalize;
        color: #000;
        font-size: 15px;
        padding: 5px 10px;
        width: inherit;
        box-sizing: border-box;
        webkit-box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3);
        -moz-box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3);
        box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3);
        -webkit-transition: all 0.15s linear;
        -moz-transition: all 0.15s linear;
        -ms-transition: all 0.15s linear;
        -o-transition: all 0.15s linear;
        transition: all 0.15s linear;
    }

    .dropdown .selected_list {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
    }

    .dropdown .selected_list .item {
        padding: 3px;
        border: 1px solid #009688;
        background-color: #009688;
        color: white;
        margin: 0 5px 5px 0;
        animation: new-item-animation .5s cubic-bezier(.65, -0.02, .72, .29);

    }

    .dropdown .selected_list .item.removed_item {
        animation: removed-item-animation .8s cubic-bezier(.65, -0.02, .72, .29);
    }

    .dropdown .selected_list .item:hover {}

    .dropdown .selected_list .item:last-child {
        margin-right: 0;
    }

    .dropdown .selected_list .item .text {
        padding: 5px;
        text-transform: capitalize;
    }

    .dropdown .selected_list .item .remove_item {
        cursor: pointer;
        padding: 0 5px;
        border-left: 1px solid white;
    }

    .dropdown .options_list {
        max-height: 100px;
        display: none;
        width: 100%;
        border: 1px solid #009688;
        border-top-width: 0;
        box-sizing: border-box;
        overflow-y: scroll;
        position: absolute;
        top: initial;
        left: 0;
        background: #fff;
        -webkit-box-shadow: 1px 1px 2px #009688;
        -moz-box-shadow: 1px 1px 2px #009688;
        box-shadow: 1px 1px 2px #009688;
        z-index: 999;

    }

    .dropdown .options_list .option {
        padding: 5px 2px 5px 10px;
        cursor: pointer;
        text-transform: capitalize; //border-bottom: 1px solid #dedede;
    }

    .dropdown .options_list .option:after {
        clear: both;
    }

    .dropdown .options_list .option:last-child {
        border-bottom: none;
    }

    .dropdown .options_list .option:hover {
        background-color: #009688;
        color: white;
    }

    .clone_elements {
        display: none;
    }

    @keyframes new-item-animation {
        0% {
            opacity: 0;
            transform: translateY(200px);
        }
        100% {
            opacity: 1;
            transform: translateY(0);
        }
    }

    @keyframes removed-item-animation {
        0% {
            opacity: 1;
            transform: translateY(0);
        }

        30% {
            opacity: .8;
            transform: translateY(200px);
        }

        80% {
            opacity: .5;
            transform: translateY(-200px);
        }

        100% {
            opacity: 0;
            transform: translateY(0px);
        }
    }

    /* placdholder color */

    .dropdown input::-webkit-input-placeholder {
        /* Chrome/Opera/Safari */
        color: white;
    }

    .dropdown input::-moz-placeholder {
        /* Firefox 19+ */
        color: white;
    }

    .dropdown input:-ms-input-placeholder {
        /* IE 10+ */
        color: white;
    }

    .dropdown input:-moz-placeholder {
        /* Firefox 18- */
        color: white;
    }

    .element-container input {
        border: 1px solid #999 !important;
        height: 30px !important;
        width: 100%;
    }

</style>

<!--    [ Strat Section Title Area]-->
<section id="login-id">
    <div class="section-paddings">
        <div class="container">
            <div class="login-all-content">
                <div class="log-frm d-table">
                    <div class="log-frm-cont d-table-cell">
                        <div class="row justify-content-center">
                            <div class="col-lg-7">
                                <div class="login-content">
                                    <div class="card">
                                        <div class="card-header text-center">
                                            <h4>WELCOME TO <img src="assets/img/prelab.png" /></h4>
                                        </div>
                                        <div class="card-bpdy">
                                            <form action="" method="post">
                                                <div class="log-frm-fild">
                                                    <div class="row justify-content-center">
                                                        <div class="col-lg-8">
                                                            <div class="row justify-content-center">
                                                                <div class="col-lg-6 text-center">
                                                                    <div class="user-pic">
                                                                        <img src="assets/img/user.png" alt="">
                                                                        <h5>Rashed Hassan</h5>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="skills-list">
                                                                        <div class="input-field">
                                                                            <i class="icofont prefix">heading</i>
                                                                            <input id="heading-title" type="text" class="validate">
                                                                            <label for="heading-title">Job Title</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <div class="skills-list">
                                                                        <div class="input-field">
                                                                            <textarea id="textarea2" class="materialize-textarea" data-length="500"></textarea>
                                                                            <label for="textarea2">Job Description</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="single-fild agree-fild">
                                                                        <div class="input-field">
                                                                            <i class="icofont prefix">money_bag</i>
                                                                            <input id="rate" type="text" class="validate">
                                                                            <label for="rate">Hourly Rate</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="single-fild agree-fild">
                                                                        <!--<div class="input-field">
                                                                            <i class="icofont prefix">location_pin</i>
                                                                            <input id="negotiate" type="text" class="validate">
                                                                            <label for="negotiate">Price Negotiable</label>
                                                                        </div>-->
                                                                        <div class="input-field">
                                                                            <i class="icofont prefix">optic</i>
                                                                            <select>
                                                                              <option value="" disabled selected>Choose option</option>
                                                                              <option value="1">Yes</option>
                                                                              <option value="2">No</option>
                                                                            </select>
                                                                            <label>Price Negotiable</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="single-fild agree-fild">
                                                                        <div class="input-field">
                                                                            <i class="icofont prefix">ui_call</i>
                                                                            <input id="contact" type="text" class="validate">
                                                                            <label for="contact">Contact</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="single-fild agree-fild">
                                                                        <div class="input-field">
                                                                            <i class="icofont prefix">location_pin</i>
                                                                            <input id="location" type="text" class="validate">
                                                                            <label for="location">Location Area</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row justify-content-center">
                                                                <div class="col-lg-4 text-center">
                                                                    <div class="single-fild">
                                                                        <button type="submit" name="login">Submit</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row justify-content-center">
                                                        <div class="col-lg-8">

                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Title Area]-->


<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->
<?php include('footer.php'); ?>
<script>
    $(document).ready(function() {
        $('input#input_text, textarea#textarea2').characterCounter();
    });

    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems, options);
    });

    // Or with jQuery

    $(document).ready(function() {
        $('select').formSelect();
    });

</script>
<script>
    $(document).ready(function() {
        $('.dropdown').dropdown({
            multi_select: true
        });
    });

    $.fn.dropdown = function(options) {
        var $input = this.find('input'),
            $options_list = this.find('.options_list'),
            $seleted_list = $options_list.siblings('.selected_list'),
            settings = $.extend({}, {
                multi_select: false
            }, options);
        $input.on('click', function() {
            $options_list
                .slideDown("fast");
        });

        $options_list.on('click', '.option', function() {
            var $selected_option = $(this),
                data_value = $(this).attr('data-value'),
                data_text = $(this).text().trim();
            $input
                .attr('data-value', data_value)
                .val(data_text);
            if (settings.multi_select) {
                var $item = $('<div class="item" data-value=""><span class="text"></span><span class="remove_item">x</span></div>');
                $item
                    .attr('data-value', data_value)
                    .find('.text')
                    .text(data_text);
                $seleted_list.append($item);
                $selected_option.remove();
                $options_list
                    .siblings('input')
                    .attr("data-value", "")
                    .val("");
            }
            $options_list.slideUp("fast");
        });

        $seleted_list.off('click').on('click', '.item .remove_item', function() {
            var $clicked_item = $(this).parents('.item'),
                item_text = $clicked_item
                .find('.text')
                .text()
                .trim(),
                item_data_value = $clicked_item.attr('data-value'),
                $item = $('<div class="option" data-value="' + item_data_value + '">' + item_text + '</div>');
            $clicked_item.addClass('removed_item');
            setTimeout(function() {
                $options_list.append($item);
                $clicked_item.remove();
            }, 500);
        });
        return this;
    }

    $.fn.selectedList = function() {
        var list = [];
        this.find('.selected_list .item').each(function(ind, option) {
            list.push({
                key: $(option).attr('data-value'),
                value: $(option).find('.text').text().trim()
            })
        });
        return list;
    };

</script>
