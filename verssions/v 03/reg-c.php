<?php include('header.php'); ?>

<!--    [Strat Section Title Area]-->
<section id="login-id">
    <div class="section-paddings">
        <div class="container">
            <div class="login-all-content">
                <div class="log-frm d-table">
                    <div class="log-frm-cont d-table-cell">
                        <div class="row justify-content-center">
                            <div class="col-lg-7">
                                <div class="login-content">
                                    <div class="card">
                                        <div class="card-header text-center ac-head">
                                            <h4>Complete you Account <span>demo.account@gmail.com</span></h4>
                                        </div>
                                        <div class="card-bpdy">
                                            <form action="" method="post">

                                                <div class="log-frm-fild">
                                                    <div class="row justify-content-center">
                                                        <div class="col-lg-8">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="single-fild agree-fild">
                                                                        <div class="input-field">
                                                                            <i class="icofont prefix">world</i>
                                                                            <input id="country" type="text" class="validate">
                                                                            <label for="country">Country</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row justify-content-center">
                                                                <div class="col-lg-12">
                                                                    <div class="single-fild">
                                                                        <div class="input-field">
                                                                            <i class="icofont prefix">ui_unlock</i>
                                                                            <input id="email" type="password" class="validate">
                                                                            <label for="email">Password</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row justify-content-center">
                                                                <div class="col-lg-6">
                                                                    <div class="single-fild">
                                                                        <div class="ac-type">
                                                                            <a href="#" class="ac-typ-control">
                                                                               <i class="icofont">handshake_deal</i>
                                                                                Hire for a Project
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="single-fild">
                                                                        <div class="ac-type">
                                                                            <a href="#" class="ac-typ-control">
                                                                               <i class="icofont">architecture_alt</i>
                                                                                Work as a Freelancer
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <div class="single-fild">
                                                                        <div class="input-field">
                                                                            <i class="icofont prefix">user_alt_3</i>
                                                                            <input id="user" type="password" class="validate">
                                                                            <label for="user">User Name</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row justify-content-center">
                                                                <div class="col-lg-12">
                                                                    <div class="single-fild agree-fild">
                                                                        <p>
                                                                            <label>
                                                                                <input type="checkbox" class="filled-in" checked="checked" />
                                                                                <span>Yes! Send me genuenly usefull email........</span>
                                                                            </label>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <div class="single-fild agree-fild">
                                                                        <p>
                                                                            <label>
                                                                                <input type="checkbox" />
                                                                                <span>Yes! I Understand and agree to the</span>
                                                                            </label>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row justify-content-center">
                                                                <div class="col-lg-4 text-center">
                                                                    <div class="single-fild">
                                                                        <button type="submit" name="login">Get Started</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Title Area]-->


<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->
<?php include('footer.php'); ?>
<script>
    $(document).ready(function() {
        $('input#input_text, textarea#textarea2').characterCounter();
    });

    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems, options);
    });

    // Or with jQuery

    $(document).ready(function() {
        $('datalist').formSelect();
    });
</script>