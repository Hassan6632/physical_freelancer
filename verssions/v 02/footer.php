<!--    [ Start Footer Area]-->
<footer>
    <div class="copy-right">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="copy-context">
                        <p>&copy; 2018. All Rights Reserved. Powred By <span><a href="http://preneurlab.com/"><img src="assets/img/prelab.png" alt=""></a></span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--    [Finish Footer Area]-->

<!--SCROLL TOP BUTTON-->
<a href="#" class="top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>




<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->

<!--    [jQuery]-->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<!--    [Popper Js] -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

<!--    [Bootstrap Js] -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"></script>

<!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>

<!--    [OwlCarousel Js]-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>

<!--    [Main Custom Js] -->
<script src="assets/js/main.js"></script>
</body>

</html>
