<?php include('header.php'); ?>

<!--    [Strat Section Title Area]-->
<section id="login-id">
    <div class="section-paddings">
        <div class="full-reg-content d-table">
            <div class="full-reg-txt d-table-cell">
                <div class="container">
                    <div class="card">
                        <div class="row justify-content-center">
                            <div class="col-lg-4">
                                <div class="single-fild">
                                    <div class="input-field">
                                        <i class="icofont prefix">user_alt_3</i>
                                        <input id="fname" type="text" class="validate">
                                        <label for="fname">First Name</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="single-fild">
                                    <div class="input-field">
                                        <i class="icofont prefix">user_alt_3</i>
                                        <input id="last-name" type="text" class="validate">
                                        <label for="last-name">Last Name</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="single-fild">
                                    <div class="input-field">
                                        <i class="icofont prefix">ui_email</i>
                                        <input id="email" type="text" class="validate">
                                        <label for="email">Email</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="single-fild">
                                    <div class="input-field">
                                        <i class="icofont prefix">mobile_phone</i>
                                        <input id="contact" type="text" class="validate">
                                        <label for="contact">Contact Number</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="single-fild">
                                    <div class="input-field">
                                        <i class="icofont prefix">id_card</i>
                                        <input id="nid" type="text" class="validate">
                                        <label for="nid">NID Number</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="single-fild">
                                    <div class="input-field">
                                        <i class="icofont prefix">location_pin</i>
                                        <input id="address" type="text" class="validate">
                                        <label for="address">Your Address</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="single-fild">
                                    <div class="input-field">
                                        <i class="icofont prefix">world</i>
                                        <input id="country" type="password" class="validate">
                                        <label for="country">Country Name</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="single-fild">
                                    <div class="input-field">
                                        <i class="icofont prefix">ui_unlock</i>
                                        <input id="password" type="password" class="validate">
                                        <label for="password">Password</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="single-fild">
                                    <div class="input-field">
                                        <i class="icofont prefix">user_alt_3</i>
                                        <input id="user" type="password" class="validate">
                                        <label for="user">User Name</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="single-fild">
                                    <div class="input-field col s12">
                                        <select>
                                              <option value="" disabled selected>Account Type</option>
                                              <option value="1">AS A FREELANCER</option>
                                              <option value="2">AS A BUYER</option>
                                            </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="single-fild">
                                    <div class="input-field col s12">
                                        <select>
                                              <option value="" disabled selected>Company Type</option>
                                              <option value="1">it's Just me</option>
                                              <option value="2">2-9 Employees</option>
                                              <option value="2">10-100 Employees</option>
                                              <option value="2">More Then 100 Employees</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="single-fild">
                                    <div class="input-field col s12">
                                        <select>
                                              <option value="" disabled selected>Working Position</option>
                                              <option value="1">it's Just me</option>
                                              <option value="2">2-9 Employees</option>
                                              <option value="2">10-100 Employees</option>
                                              <option value="2">More Then 100 Employees</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="single-fild">
                                    <div class="input-field">
                                        <i class="icofont prefix">ui_unlock</i>
                                        <textarea id="description" class="materialize-textarea"></textarea>
                                        <label for="description">Description</label>
                                    </div>

                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="single-fild">
                                    <div class="file-field input-field">
                                        <div class="btn">
                                            <span>Profile Image</span>
                                            <input type="file">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-4 text-center">
                                <div class="single-fild">
                                    <button type="submit" name="login">Get Started</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Title Area]-->


<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->
<?php include('footer.php'); ?>
<script>
    $(document).ready(function() {
        $('input#input_text, textarea#textarea2').characterCounter();
    });

    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems, options);
    });

    // Or with jQuery

    $(document).ready(function() {
        $('datalist').formSelect();
    });

    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems, options);
    });

    // Or with jQuery

    $(document).ready(function() {
        $('select').formSelect();
    });



    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#test').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#tstpic").change(function() {
            readURL(this);
        }

    );

</script>
