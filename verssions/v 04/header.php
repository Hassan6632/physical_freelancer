<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--    [Site Title] -->
    <title>Title</title>

    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">

    <!--    [Bootstrap Css] -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css">

    <!--    [Animate Css]-->
    <link rel="stylesheet" href="https://raw.githubusercontent.com/daneden/animate.css/master/animate.css">

    <!--    [FontAwesome Css] -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css">

    <!--    [IcoFont Css] -->
    <link rel="stylesheet" href="assets/css/icofont.css">

    <!--    [OwlCarousel Css]-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">

    <!--    [Custom Stlesheet]-->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">


    <!--    [Favicon] -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">


</head>

<body>
    <!--PRELOADER START-->

    <!--    [ Strat Preloader Area]-->
    <!--<div class="pre-loader-area">
    <div class="sk-folding-cube">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
    </div>
</div>-->


    <!--    [Finish Poreloader Area]-->
    <!--    [ Strat Header Area]-->
    <header>
        <!--    [ Strat Logo Area]-->
        <div class="main-menu">
            <div class="container">
                <div class="top-menu">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                        <a href="/" class="navbar-brand">Brand</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar6">
        <span class="navbar-toggler-icon"></span>
    </button>
                        <div class="navbar-collapse collapse justify-content-stretch" id="navbar6">
                            <ul class="navbar-nav">
                                <li class="nav-item active">
                                    <a class="nav-link" href="#">Link <span class="sr-only">Home</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Link</a>
                                </li>
                            </ul>
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Link</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Codeply</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Link</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
        <!--    [Finish Logo Area]-->
    </header>
    <!--    [Finish Header Area]-->
