<?php include('header.php'); ?>
<section id="job-view">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="single-job-view">
                        <div class="card">
                            <div class="card-header">
                                <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>
                            </div>
                            <div class="card-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis aut quam debitis deserunt ut impedit officia doloribus eos ducimus? Reiciendis nesciunt est magni quasi maxime harum quam, sapiente adipisci nihil totam, quo! Magni delectus, quidem quo quibusdam illo fugiat possimus animi quam dolorem tenetur inventore mollitia reiciendis soluta voluptate ipsam!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis aut quam debitis deserunt ut impedit officia doloribus eos ducimus? Reiciendis nesciunt est magni quasi maxime harum quam, sapiente adipisci nihil totam, quo! Magni delectus, quidem quo quibusdam illo fugiat possimus animi quam dolorem tenetur inventore mollitia reiciendis soluta voluptate ipsam!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis aut quam debitis deserunt ut impedit officia doloribus eos ducimus? Reiciendis nesciunt est magni quasi maxime harum quam, sapiente adipisci nihil totam, quo! Magni delectus, quidem quo quibusdam illo fugiat possimus animi quam dolorem tenetur inventore mollitia reiciendis soluta voluptate ipsam!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis aut quam debitis deserunt ut impedit officia doloribus eos ducimus? Reiciendis nesciunt est magni quasi maxime harum quam, sapiente adipisci nihil totam, quo! Magni delectus, quidem quo quibusdam illo fugiat possimus animi quam dolorem tenetur inventore mollitia reiciendis soluta voluptate ipsam!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis aut quam debitis deserunt ut impedit officia doloribus eos ducimus? Reiciendis nesciunt est magni quasi maxime harum quam, sapiente adipisci nihil totam, quo! Magni delectus, quidem quo quibusdam illo fugiat possimus animi quam dolorem tenetur inventore mollitia reiciendis soluta voluptate ipsam!</p>
                            </div>
                            <div class="card-footer">
                                <div class="job-type-info">
                                    <h6>Job Type: One Time</h6>
                                    <h6>Rate Type: Hourly</h6>
                                    <h6>Skill Query: Expert</h6>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="proposel">

                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->
<?php include('footer.php'); ?>
