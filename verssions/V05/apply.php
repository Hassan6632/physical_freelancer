<?php include('header.php'); ?>
<!--    [ Strat Section Area]-->
<section id="login-id">
    <div class="section-paddings">
        <div class="container">
            <div class="login-all-content">
                <div class="log-frm d-table">
                    <div class="log-frm-cont d-table-cell">
                        <div class="row justify-content-center">
                            <div class="col-lg-7">
                                <div class="login-content">
                                    <div class="card">
                                        <div class="card-header text-center">
                                            <h4>WELCOME TO <img src="assets/img/prelab.png" /></h4>
                                        </div>
                                        <div class="card-bpdy">
                                            <form action="" method="post">
                                                <div class="log-frm-fild">
                                                    <div class="row justify-content-center">
                                                        <div class="col-lg-8">

                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="skills-list">
                                                                        <div class="input-field">
                                                                            <textarea id="textarea2" class="materialize-textarea" data-length="500"></textarea>
                                                                            <label for="textarea2">Proposal Latter</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row justify-content-center">
                                                                <div class="col-lg-6">
                                                                    <div class="single-fild agree-fild">
                                                                        <div class="input-field">
                                                                            <i class="icofont prefix">money_bag</i>
                                                                            <input id="rate" type="text" class="validate">
                                                                            <label for="rate">Rate</label>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-lg-6">
                                                                    <div class="single-fild agree-fild">
                                                                        <div class="input-field">
                                                                            <i class="icofont prefix">medal</i>
                                                                            <input id="experiance" type="text" class="validate">
                                                                            <label for="experiance">Experiance</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row justify-content-center">
                                                                <div class="col-lg-4 text-center">
                                                                    <div class="single-fild">
                                                                        <button type="submit" name="login">Submit</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row justify-content-center">
                                                        <div class="col-lg-8">

                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->
<?php include('footer.php'); ?>
