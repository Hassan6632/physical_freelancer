<?php include('header.php'); ?>

<!--    [ Strat Section Title Area]-->
<section id="login-id">
    <div class="section-paddings">
        <div class="container">
            <div class="login-all-content">
                <div class="log-frm d-table">
                    <div class="log-frm-cont d-table-cell">
                        <div class="row justify-content-center">
                            <div class="col-lg-6">
                                <div class="login-content">
                                    <div class="card">
                                        <div class="card-header text-center log-pass-title">
                                            <h4>WELCOME TO <span>a6185711@nwytg.net</span></h4>
                                        </div>
                                        <div class="card-bpdy">
                                            <form action="" method="post">
                                                <div class="log-frm-fild">
                                                    <div class="row justify-content-center">
                                                        <div class="col-lg-8 col-8">
                                                            <div class="row">
                                                                <div class="col-lg-4">
                                                                    <div class="single-fild">
                                                                        <label for="user">Password</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                    <div class="single-fild">
                                                                        <input id="user" type="password" name="password" placeholder="" required />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="single-fild">
                                                                        <p>
                                                                            <label>
                                                                                <input type="checkbox" />
                                                                                <span>Keep me login</span>
                                                                            </label>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="single-fild forgot-pass">
                                                                        <a href="#">Forgot Password?</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row justify-content-center">
                                                                <div class="col-lg-4 text-center">
                                                                    <div class="single-fild log-pass">
                                                                        <button type="submit" name="login">LogIn</button>
                                                                    </div>
                                                                    <div class="single-fild log-pass">
                                                                        <a href="#">Not You</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Title Area]-->


<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->
<?php include('footer.php'); ?>
