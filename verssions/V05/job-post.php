<?php include('header.php'); ?>


<!--    [ Strat Section Title Area]-->
<section id="login-id">
    <div class="section-paddings">
        <div class="container">
            <div class="login-all-content">
                <div class="log-frm d-table">
                    <div class="log-frm-cont d-table-cell">
                        <div class="row justify-content-center">
                            <div class="col-lg-7">
                                <div class="login-content">
                                    <div class="card">
                                        <div class="card-header text-center">
                                            <h4>WELCOME TO <img src="assets/img/prelab.png" /></h4>
                                        </div>
                                        <div class="card-bpdy">
                                            <form action="" method="post">
                                                <div class="log-frm-fild">
                                                    <div class="row justify-content-center">
                                                        <div class="col-lg-8">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="skills-list">
                                                                        <div class="input-field">
                                                                            <i class="icofont prefix">heading</i>
                                                                            <input id="heading-title" type="text" class="validate">
                                                                            <label for="heading-title">Job Title</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <div class="skills-list">
                                                                        <div class="input-field">
                                                                            <textarea id="textarea2" class="materialize-textarea" data-length="500"></textarea>
                                                                            <label for="textarea2">Job Description</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <div class="skills-list">
                                                                        <div class="input-field col s12">
                                                                            <select>
                                                                              <option value="" disabled selected>Choose option</option>
                                                                              <option value="1">One Time</option>
                                                                              <option value="2">Project Basis</option>
                                                                              <option value="3">Long Time</option>
                                                                            </select>
                                                                            <label>Job Type</label>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="row justify-content-center">
                                                                <div class="col-lg-4 text-center">
                                                                    <div class="single-fild">
                                                                        <button type="submit" name="login">Submit</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Title Area]-->


<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->
<?php include('footer.php'); ?>
<script>
    $(document).ready(function() {
        $('input#input_text, textarea#textarea2').characterCounter();
    });

    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems, options);
    });

    // Or with jQuery

    $(document).ready(function() {
        $('select').formSelect();
    });

</script>
<script>
    $(document).ready(function() {
        $('.dropdown').dropdown({
            multi_select: true
        });
    });

    $.fn.dropdown = function(options) {
        var $input = this.find('input'),
            $options_list = this.find('.options_list'),
            $seleted_list = $options_list.siblings('.selected_list'),
            settings = $.extend({}, {
                multi_select: false
            }, options);
        $input.on('click', function() {
            $options_list
                .slideDown("fast");
        });

        $options_list.on('click', '.option', function() {
            var $selected_option = $(this),
                data_value = $(this).attr('data-value'),
                data_text = $(this).text().trim();
            $input
                .attr('data-value', data_value)
                .val(data_text);
            if (settings.multi_select) {
                var $item = $('<div class="item" data-value=""><span class="text"></span><span class="remove_item">x</span></div>');
                $item
                    .attr('data-value', data_value)
                    .find('.text')
                    .text(data_text);
                $seleted_list.append($item);
                $selected_option.remove();
                $options_list
                    .siblings('input')
                    .attr("data-value", "")
                    .val("");
            }
            $options_list.slideUp("fast");
        });

        $seleted_list.off('click').on('click', '.item .remove_item', function() {
            var $clicked_item = $(this).parents('.item'),
                item_text = $clicked_item
                .find('.text')
                .text()
                .trim(),
                item_data_value = $clicked_item.attr('data-value'),
                $item = $('<div class="option" data-value="' + item_data_value + '">' + item_text + '</div>');
            $clicked_item.addClass('removed_item');
            setTimeout(function() {
                $options_list.append($item);
                $clicked_item.remove();
            }, 500);
        });
        return this;
    }

    $.fn.selectedList = function() {
        var list = [];
        this.find('.selected_list .item').each(function(ind, option) {
            list.push({
                key: $(option).attr('data-value'),
                value: $(option).find('.text').text().trim()
            })
        });
        return list;
    };

</script>
