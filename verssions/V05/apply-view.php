<?php include('header.php'); ?>

<!--    [ Strat Section Title Area]-->
<section id="job-view">
    <div class="section-padding">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-2">
                    <div class="jobview-bar">
                        <h6>Search By Topic</h6>
                        <div class="single-search-fild">
                            <p>
                                <label>
                                    <input type="checkbox" />
                                    <span>IT Jobs</span>
                                </label>
                            </p>
                        </div>
                        <div class="single-search-fild">
                            <p>
                                <label>
                                    <input type="checkbox" />
                                    <span>Business Job</span>
                                </label>
                            </p>
                        </div>
                        <div class="single-search-fild">
                            <p>
                                <label>
                                    <input type="checkbox" />
                                    <span>Electronics Job</span>
                                </label>
                            </p>
                        </div>
                        <div class="single-search-fild">
                            <p>
                                <label>
                                    <input type="checkbox" />
                                    <span>Red</span>
                                </label>
                            </p>
                        </div>
                        <div class="single-search-fild">
                            <p>
                                <label>
                                    <input type="checkbox" />
                                    <span>Red</span>
                                </label>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="all-job-view">
                        <div class="single-job-view">
                            <a href="">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="row margin-btm">
                                            <div class="col-lg-8 text-left">
                                                <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>
                                            </div>
                                            <div class="col-lg-4 text-right">
                                                <div class="user-out">
                                                    <p>Signed in as Rashed Hasan</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis aut quam debitis deserunt ut impedit officia doloribus eos ducimus? Reiciendis nesciunt est magni quasi maxime harum quam, sapiente adipisci nihil totam, quo! Magni delectus, quidem quo quibusdam illo fugiat possimus animi quam dolorem tenetur inventore mollitia reiciendis soluta voluptate ipsam!</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="single-job-view">
                            <a href="">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="row margin-btm">
                                            <div class="col-lg-8 text-left">
                                                <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>
                                            </div>
                                            <div class="col-lg-4 text-right">
                                                <div class="user-out">
                                                    <p>Signed in as Rashed Hasan</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis aut quam debitis deserunt ut impedit officia doloribus eos ducimus? Reiciendis nesciunt est magni quasi maxime harum quam, sapiente adipisci nihil totam, quo! Magni delectus, quidem quo quibusdam illo fugiat possimus animi quam dolorem tenetur inventore mollitia reiciendis soluta voluptate ipsam!</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="single-job-view">
                            <a href="">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="row margin-btm">
                                            <div class="col-lg-8 text-left">
                                                <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>
                                            </div>
                                            <div class="col-lg-4 text-right">
                                                <div class="user-out">
                                                    <p>Signed in as Rashed Hasan</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis aut quam debitis deserunt ut impedit officia doloribus eos ducimus? Reiciendis nesciunt est magni quasi maxime harum quam, sapiente adipisci nihil totam, quo! Magni delectus, quidem quo quibusdam illo fugiat possimus animi quam dolorem tenetur inventore mollitia reiciendis soluta voluptate ipsam!</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="single-job-view">
                            <a href="">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="row margin-btm">
                                            <div class="col-lg-8 text-left">
                                                <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>
                                            </div>
                                            <div class="col-lg-4 text-right">
                                                <div class="user-out">
                                                    <p>Signed in as Rashed Hasan</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis aut quam debitis deserunt ut impedit officia doloribus eos ducimus? Reiciendis nesciunt est magni quasi maxime harum quam, sapiente adipisci nihil totam, quo! Magni delectus, quidem quo quibusdam illo fugiat possimus animi quam dolorem tenetur inventore mollitia reiciendis soluta voluptate ipsam!</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="single-job-view">
                            <a href="">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="row margin-btm">
                                            <div class="col-lg-8 text-left">
                                                <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>
                                            </div>
                                            <div class="col-lg-4 text-right">
                                                <div class="user-out">
                                                    <p>Signed in as Rashed Hasan</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis aut quam debitis deserunt ut impedit officia doloribus eos ducimus? Reiciendis nesciunt est magni quasi maxime harum quam, sapiente adipisci nihil totam, quo! Magni delectus, quidem quo quibusdam illo fugiat possimus animi quam dolorem tenetur inventore mollitia reiciendis soluta voluptate ipsam!</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="user-symmery">
                        <div class="card">
                            <div class="card-header">
                                <h4>Job Apply Summery</h4>
                            </div>
                            <div class="card-body">
                                <h5>Total Submit: 60</h5>
                                <h5>Apply Job: 60</h5>
                                <h5>To Do: 60</h5>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--    [Finish Section Title Area]-->


<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->
<?php include('footer.php'); ?>
