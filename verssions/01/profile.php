<?php include('header.php'); ?>

<!--    [ Strat Section Title Area]-->
<section id="login-id">
    <div class="section-paddings">
        <div class="container">
            <div class="login-all-content">
                <div class="log-frm d-table">
                    <div class="log-frm-cont d-table-cell">
                        <div class="row justify-content-center">
                            <div class="col-lg-7">
                                <div class="login-content">
                                    <div class="card">
                                        <div class="card-header text-center">
                                            <div class="row margin-btm">
                                                <div class="col-lg-6 text-left">
                                                    <h4><img src="assets/img/prelab.png" /></h4>
                                                </div>
                                                <div class="col-lg-6 text-right">
                                                    <div class="user-out">
                                                        <p>Signed in as Rashed Hasan<a href="">Sign Out</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-bpdy">
                                            <form action="" method="post">

                                                <div class="log-frm-fild">
                                                    <div class="row justify-content-center">
                                                        <div class="col-lg-8">
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="single-fild">
                                                                        <div class="input-field">
                                                                            <i class="icofont prefix">user_alt_3</i>
                                                                            <h5></h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="single-fild">
                                                                        <div class="input-field">
                                                                            <i class="icofont prefix">user_alt_3</i>
                                                                            <input id="last-name" type="text" class="validate">
                                                                            <label for="last-name">Last Name</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row justify-content-center">
                                                                <div class="col-lg-12">
                                                                    <div class="single-fild">
                                                                        <div class="input-field">
                                                                            <i class="icofont prefix">ui_email</i>
                                                                            <input id="email" type="text" class="validate">
                                                                            <label for="email">Email</label>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="row justify-content-center">
                                                                <div class="col-lg-4 text-center">
                                                                    <div class="single-fild">
                                                                        <button type="submit" name="login">Get Started</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Title Area]-->


<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->
<?php include('footer.php'); ?>
<script>
    $(document).ready(function() {
        $('input#input_text, textarea#textarea2').characterCounter();
    });

</script>
